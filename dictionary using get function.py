# functions


def user_input_male():
    return "User is a male."


def user_input_tall():
    return "User is tall.\n"


def check_point1():
    try:
        inputs1 = user_dict_TorF.get(The_input1)
        if inputs1:
            my_output = user_input_male()
            print(my_output)
        else:
            print("User is not a male.")
    except ValueError:
        print("Not a valid input.")


def check_point2():
    try:
        inputs2 = user_dict_TorF.get(The_input2)
        if inputs2:
            my_output = user_input_tall()
            print(my_output)
        else:
            print("User is not tall.\n")
    except ValueError:
        print("Not a valid input.")


The_input1 = ""
The_input2 = ""
while The_input1 != "exit" and The_input2 != "exit":
    The_input1 = str(input("Is the User a male?\n'T' or 't' = True, 'F' or 'f' = False:\n"))
    The_input2 = str(input("Is the User tall?\n'T' or 't' = True, 'F' or 'f' = False:\n"))
    user_dict_TorF = {"T": True, "t": True, "F": False, "f": False}
    check_point1()
    check_point2()
