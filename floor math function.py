from math import *

my_value = input("enter a number:\n")

'''my_value = input("enter a number:\n")
print(floor(my_value))'''

# We get an error (ValueError: invalid literal for int() with base 10:)

# This error is caused because we try to convert “x.x: to an integer.
# The value “x.x” is formatted as a string.
# Python cannot convert a floating-point number in a string to an integer.
# To overcome this issue, we need to convert the value a user inserts to a floating point number.
#  Then, we can convert it to an integer.

valuee = int(float(my_value))
print(floor(valuee))

